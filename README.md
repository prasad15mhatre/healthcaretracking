## Instructions to run locally

1) Clone repository and download npm packages 

```
git clone https://bitbucket.org/prasad15mhatre/healthcaretracking
npm install
```

2) Launch mongod in one terminal then run server.js

````
mongod
node server.js
````

3) Open browser `http://localhost:3000/`

Special thanks to Ahmed Haque (afhaque@rice.edu) for his valuable work at https://github.com/scotch-io/mean-google-maps.
    